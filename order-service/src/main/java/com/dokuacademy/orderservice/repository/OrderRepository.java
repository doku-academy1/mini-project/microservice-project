package com.dokuacademy.orderservice.repository;

import com.dokuacademy.orderservice.entities.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {
    List<Order> findAllByTransactionId(Long transactionId);

}
