package com.dokuacademy.orderservice.entities.request;

import lombok.*;

import java.io.Serializable;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderRequest implements Serializable {
    private String status;
    private Integer qty;
    private Double amount;
    private Long transactionId;
}
