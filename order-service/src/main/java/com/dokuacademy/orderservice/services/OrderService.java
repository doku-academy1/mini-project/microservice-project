package com.dokuacademy.orderservice.services;

import com.dokuacademy.orderservice.entities.Order;
import com.dokuacademy.orderservice.entities.request.OrderRequest;
import com.dokuacademy.orderservice.repository.OrderRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService {

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    ModelMapper modelMapper;

    public List<Order> getAllOrderByTransactionId(Long transactionId) {
        List<Order> listOrderByTransactionId = orderRepository.findAllByTransactionId(transactionId);
        return listOrderByTransactionId;
    }

    public Order createOrder(OrderRequest orderRequest) {
        Order order = convertToEntity(orderRequest);
        Order createdOrder = orderRepository.save(order);
        return createdOrder;
    }

    public Order updateOrder(OrderRequest orderRequest, Long orderId) {
        Order orders = convertToEntity(orderRequest);
        orderRepository.findById(orderId).map(order -> {
            order.setQty(orders.getQty());
            order.setAmount(orders.getAmount());
            order.setStatus(orders.getStatus());
            order.setTransactionId(orders.getTransactionId());
            return orderRepository.save(order);
        }).orElseThrow(() -> {throw new RuntimeException();});

        return orders;
    }

    public void deleteOrder(Long orderId) {
        Optional<Order> order = orderRepository.findById(orderId);
        if (!order.isPresent()) {
            throw new RuntimeException();
        }
        orderRepository.deleteById(orderId);
    }

    private Order convertToEntity(OrderRequest orderRequest) {
        return modelMapper.map(orderRequest, Order.class);
    }

    private OrderRequest convertToRequest(Order order) {
        return modelMapper.map(order, OrderRequest.class);
    }
}
